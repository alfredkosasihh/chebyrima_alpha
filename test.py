import numpy as np
import pandas as pd
from keras.models import Sequential
from keras.layers import Dense, Dropout, Activation, Flatten, LSTM, TimeDistributed, RepeatVector
from keras.layers.normalization import BatchNormalization
from keras.optimizers import Adam
from keras.callbacks import EarlyStopping, ModelCheckpoint
import tensorflow as tf
from datetime import datetime, timedelta
import os

class ChebyLSTM:
    target_field = 'Temp'

    def __init__(self, filename):
        df = pd.read_csv(filename)
        self.data = df.iloc[:30000].copy()       

    def normalization(self):
        self.data = self.data.round(2)
    
    def timeParser(self):
        data = self.data['LocalTime'].copy()
        time = []           
        for i in range(data.shape[0]):
            time_split = data.iloc[i].split(' ')
            date_split = time_split[0].split('/')
            year = '20'+date_split[2]
            month = date_split[0]
            day = date_split[1]
            today = year+'-'+month+'-'+day+' '+time_split[1]
            time.append(datetime.strptime(today, '%Y-%m-%d %H:%M:%S'))
        self.data['LocalTime'] = pd.Series(time)
        self.data = self.data.set_index('LocalTime') 

    def newField(self):
        self.data[self.target_field + '_m'] = [np.nan] * self.data.shape[0]

    def outputToCsv(self, filename):
        self.data.to_csv(filename)

    def anomalyDetection(self, method = 'chebyshev', value_maximun = 40, value_minimun = 0):

        def timeParser(date, time, day = 0):
            _date = (date + timedelta(days = day)).strftime('%Y-%m-%d')
            return datetime.strptime(_date + ' ' + time, '%Y-%m-%d %H:%M:%S')

        self.data[self.target_field][self.data[self.target_field] > value_maximun] = np.nan
        self.data[self.target_field][self.data[self.target_field] < value_minimun] = np.nan
        if method == 'chebyshev':
            start_day = self.data.index[0]
            end_day = self.data.index[-1]
            days = 0
            run = True
            while run:
                if days == 0:
                    start_time = start_day
                    end_time = timeParser(start_time.date(), '23:55:00')
                elif timeParser(start_day.date(), '00:00:00', day = days).date() == end_day.date():
                    start_time = timeParser(start_day.date(), '00:00:00', day = days)
                    end_time = end_day    
                    run = False
                else:
                    start_time = timeParser(start_day.date(), '00:00:00', day = days)
                    end_time = timeParser(start_day.date(), '23:55:00', day = days)                
                avg = self.data.loc[start_time:end_time][self.target_field].mean()
                std = self.data.loc[start_time:end_time][self.target_field].std()
                std *= 2
                self.data.loc[start_time:end_time][self.target_field][self.data[self.target_field] > (avg + std)] = np.nan
                self.data.loc[start_time:end_time][self.target_field][self.data[self.target_field] < (avg - std)] = np.nan
                std = self.data.loc[start_time:end_time][self.target_field].std()
                std *= 4
                self.data.loc[start_time:end_time][self.target_field][self.data[self.target_field] > (avg + std)] = np.nan
                self.data.loc[start_time:end_time][self.target_field][self.data[self.target_field] < (avg - std)] = np.nan
                self.condition = np.isnan(self.data[self.target_field])
                self.condition = list(self.condition)
                days += 1

    def buildTrain(self, past_day=24, futureDay=1):
        self.x_train = []
        self.y_train = []
        for i in range(past_day+futureDay, self.data.shape[0]):
            if self.data[self.target_field][i-past_day-futureDay:i].count() < past_day+futureDay:
                pass
            else:
                x = np.array(self.data[self.target_field].iloc[i-past_day-futureDay:i-futureDay])
                x = x.reshape(x.shape[0], 1)
                self.x_train.append(x)
                self.y_train.append(np.array(self.data.iloc[i-futureDay:i][self.target_field]))
        self.x_train = np.array(self.x_train)
        self.y_train = np.array(self.y_train)
        print(self.x_train)
        print(self.y_train)

    def shuffle(self, seed=10):
        np.random.seed(seed)
        randomList = np.arange(self.x_train.shape[0])
        np.random.shuffle(randomList)
        self.x_train = self.x_train[randomList]
        self.y_train = self.y_train[randomList]

    def buildModel(self):
        self.model = Sequential()
        print(self.x_train.shape)
        self.model.add(LSTM(10, input_length=self.x_train.shape[1], input_dim=1))
        self.model.add(Dense(1))
        self.model.compile(loss="mse", optimizer="adam")
        self.model.summary()
        callback = EarlyStopping(monitor="loss", patience=10, verbose=1, mode="auto")
        self.x_train = self.x_train.astype(np.float32)
        self.y_train = self.y_train.astype(np.float32)
        self.model.fit(self.x_train, self.y_train, epochs=1000, batch_size=128, callbacks=[callback])

    def modelSave(self, model_name='./models/lstm_model.h5'):
        self.model.save(model_name)

    def modelLoad(self, model_name='./models/lstm_model.h5'):
        self.model = tf.keras.models.load_model(model_name)
        self.model.summary()

    def correction(self, past_day = 24):
        s = 0
        for times in range(int(past_day/2)):
            mask = list(np.isnan(self.data[self.target_field]))
            for i in range(past_day+1, len(mask)):
                if mask[i]:
                    if mask[i-past_day:i].count(True) > 0:
                        pass
                    else:
                        s += 1
                        # self.test = []
                        # x = np.array(self.data[self.target_field].iloc[i-past_day:i])
                        # x = x.reshape(x.shape[0], 1)
                        # self.test.append(x)
                        # self.test = np.array(self.test)
                        # y_hat = self.model.predict(self.test)
                        # self.data[self.target_field][self.data.index[i]] = y_hat[0]
        return s
            
    def smooth(self, gap = 0.3, fix = 48):
        a_array = self.data[self.target_field].copy().tolist()
        b_array = a_array.copy()
        b_array.pop()
        b_array.insert(0, 0)
        diff_value = np.array(a_array) - np.array(b_array)
        diff_array = pd.Series((diff_value).tolist())
        positive_mask = diff_array >= gap
        negative_mask = diff_array <= -gap
        for i in range(1, diff_array.shape[0]):
            if positive_mask[i] or negative_mask[i]:
                if diff_value[i] > 0:
                    n = diff_value[i] / 2
                    x = n / fix
                    for j in range(fix):
                        k = fix - j
                        try:
                            if not np.isnan(self.data.iloc[i+j][self.target_field]):
                                self.data[self.target_field][self.data.index[i + j]] -= x*k
                        except Exception as e:
                            pass
                        try:
                            if not np.isnan(self.data.iloc[i-j-1][self.target_field]):
                                self.data[self.target_field][self.data.index[i-j-1]] += x*k
                        except Exception as e:
                            pass                                 
                elif diff_value[i] < 0:
                    n = -diff_value[i] / 2
                    x = n / fix
                    for j in range(fix):
                        k = fix - j
                        try:
                            if not np.isnan(self.data.iloc[i+j][self.target_field]):
                                self.data[self.target_field][self.data.index[i + j]] += x*k
                        except Exception as e:
                            pass
                        try:
                            if not np.isnan(self.data.iloc[i-j-1][self.target_field]):
                                self.data[self.target_field][self.data.index[i-j-1]] -= x*k
                        except Exception as e:
                            pass
                       
if __name__ == "__main__":
    f = os.listdir('./initialize')
 
    cl =ChebyLSTM('./initialize/1.綜合氣象站_原台南州廳.csv')
    cl.outputToCsv('./initialize/test.csv')
    
