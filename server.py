from statsmodels.tsa.statespace.sarimax import SARIMAX
from datetime import datetime, timedelta
from keras.models import Sequential
from keras.layers import Dense, Dropout, Activation, Flatten, LSTM, TimeDistributed, RepeatVector
from keras.layers.normalization import BatchNormalization
from keras.optimizers import Adam
from keras.callbacks import EarlyStopping, ModelCheckpoint
from flask import Flask, request, render_template, jsonify, redirect
from imputpredicter.chebyrima import Chebyrima as cr
from imputpredicter.chebyLSTM import ChebyLSTM as cl
import numpy as np
import pandas as pd
import time
import tensorflow as tf
import os
import json
import shutil
import sys
import requests

app = Flask(__name__)


def merge(filename, folder_name, file_path, method):
    initial_data = './files/' + file_path + '/' + filename
    chebychev_data = './anomalyDetection/' + file_path + '/' + filename
    arima = './correctionARIMA/' + file_path + '/' + filename
    lstm = './correctionLSTM/' + file_path + '/' + filename
    data1 = pd.read_csv(initial_data)
    data2 = pd.read_csv(chebychev_data)
    if method == 'ARIMA':
        data3 = pd.read_csv(arima)
    elif method == 'LSTM':
        data3 = pd.read_csv(lstm)
    else:
        return 'no file!'

    for i in ['Temp', 'Hum']:
        data1[i].iloc[np.isnan(data1[i])] = '\\N'
        data2[i].iloc[np.isnan(data2[i])] = '\\N'
        data3[i].iloc[np.isnan(data3[i])] = '\\N'

    output = pd.DataFrame()
    output['localtime'] = data2['LocalTime'].copy()
    output['initial_temp'] = data1['Temp'].copy()
    output['initial_hum'] = data1['Hum'].copy()
    output['chebychev_temp'] = data2['Temp'].copy()
    output['chebychev_hum'] = data2['Hum'].copy()
    if method == 'ARIMA':
        try:
            output['arima_temp'] = data3['Temp'].copy()
            output['temp_tag'] = data3['Temp_m'].copy()
        except:
            output['arima_temp'] = ['\\N'] * data2.shape[0]
            output['temp_tag'] = [np.nan] * data2.shape[0]
        try:
            output['arima_hum'] = data3['Hum'].copy()
            output['hum_tag'] = data3['Hum_m'].copy()
        except:
            output['arima_hum'] = ['\\N'] * data2.shape[0]
            output['hum_tag'] = [np.nan] * data2.shape[0]
        output['lstm_temp'] = ['\\N'] * data2.shape[0]
        output['lstm_hum'] = ['\\N'] * data2.shape[0]
    else:
        output['arima_temp'] = ['\\N'] * data2.shape[0]
        output['temp_tag'] = ['np.nan'] * data2.shape[0]
        output['arima_hum'] = ['\\N'] * data2.shape[0]

        output['hum_tag'] = [np.nan] * data2.shape[0]
        output['lstm_temp'] = data3['Temp'].copy()
        output['lstm_hum'] = data3['Hum'].copy()
    output['empty'] = [1] * data2.shape[0]
    names = './outcome3/' + folder_name + '_location_'+filename
    output.to_csv(names, index=False)

    if method == 'ARIMA':
        try:
            data3 = data3.drop(['Temp_m'], axis=1)
        except:
            pass
        try:
            data3 = data3.drop(['Hum_m'], axis=1)
        except:
            pass
    for i in ['Temp', 'Hum']:
        data3[i].iloc[data3[i] == '\\N'] = np.nan
    data3.to_csv('./merge/' + folder_name + '_location_'+filename, index=False)


def correctionARIMA(path, filename, field):
    data = cr('./files/'+path+filename)
    data.timeParser()
    data.normalization()
    for i in field:
        data.target_field = i
        if data.target_field == 'Temp':
            data.anomalyDetection()
        elif data.target_field == 'Hum':
            data.anomalyDetection(value_maximun=100, value_minimun=30)
    data.outputToCsv('./anomalyDetection/'+path+filename)
    for i in field:
        data.target_field = i
        data.newField()
        data.correctionVertical()
        data.reverse()
        data.correctionVertical()
        data.reverse()
        data.correctionHorizontal()
        data.reverse()
        data.correctionHorizontal()
        data.reverse()
    for i in field:
        data.target_field = i
        if data.target_field == 'Temp':
            data.smooth()
        elif data.target_field == 'Hum':
            data.smooth(gap=3)
    data.normalization()
    data.outputToCsv('./correctionARIMA/'+path+filename)


def correctionLSTM(path, filename, field):
    data = cl('./files/'+path+filename)
    data.normalization()
    data.timeParser()
    for i in field:
        data.target_field = i
        if data.target_field == 'Temp':
            data.anomalyDetection()
        elif data.target_field == 'Hum':
            data.anomalyDetection(value_maximun=100, value_minimun=30)
        data.outputToCsv('./anomalyDetection/'+path+filename)
        if data.target_field == 'Temp':
            data.modelLoad('./models/lstm_model.h5')
        elif data.target_field == 'Hum':
            data.modelLoad('./models/HumModel.h5')
        data.correction()
        if data.target_field == 'Temp':
            data.smooth()
        elif data.target_field == 'Hum':
            data.smooth(gap=3)
    data.normalization()
    data.outputToCsv('./correctionLSTM/'+path+filename)


def update_file_quantity():
    quantity = open('./config/file_quantity.json', mode="r", encoding="utf-8")
    file_quantity = json.loads(quantity.read())
    quantity.close()
    quantity = open('./config/file_quantity.json', mode="w", encoding="utf-8")
    file_quantity['file_quantity'] = int(file_quantity['file_quantity']) + 1
    quantity.write(json.dumps(file_quantity))
    quantity.close()
    return str(file_quantity['file_quantity'])


@app.route('/upload/', methods=['POST'])
def upload():
    folder_name = request.form['file_name']
    target_field = request.form.getlist('target_field')
    method = request.form['method']
    upload_time = time.strftime("%Y-%m-%d_%H:%M:%S", time.localtime())
    file_path = folder_name + '_' + upload_time

    file_quantity = update_file_quantity()
    file_path = file_quantity + '_' + file_path

    if not os.path.isdir('./files/' + file_path):
        os.makedirs('./files/' + file_path)
    if not os.path.isdir('./anomalyDetection/' + file_path):
        os.makedirs('./anomalyDetection/' + file_path)
    if method == 'ARIMA':
        if not os.path.isdir('./correctionARIMA/' + file_path):
            os.makedirs('./correctionARIMA/' + file_path)
    elif method == 'LSTM':
        if not os.path.isdir('./correctionLSTM/' + file_path):
            os.makedirs('./correctionLSTM/' + file_path)

    file = request.files['file']
    filename = file_quantity + '.csv'
    if file:

        file.save(os.path.join('./files/' + file_path, filename))
    print(folder_name, file_path)
    if method == 'ARIMA':
        correctionARIMA(file_path+'/', filename, target_field)
    elif method == 'LSTM':
        correctionLSTM(file_path+'/', filename, target_field)

    merge(filename, folder_name, file_path, method)

    shutil.rmtree('./files/' + file_path)
    shutil.rmtree('./anomalyDetection/' + file_path)
    if method == 'ARIMA':
        shutil.rmtree('./correctionARIMA/' + file_path)
    elif method == 'LSTM':
        shutil.rmtree('./correctionLSTM/' + file_path)

    import_file_name = folder_name + '_location_'+filename
    a = requests.get('http://10.20.1.231/cultural/create_table.php?name=' +
                     folder_name+'&location=location'+file_quantity)
    if a.status_code == 200:
        requests.get('http://10.20.1.231/cultural/import_table.php?name=' +
                     import_file_name+'&location=location'+file_quantity)

    return 'done'


@app.route('/index/')
def index():
    return render_template('home.html')


@app.route('/crazy/')
def ansi():
    return redirect('http://10.20.1.231:3000/d/bTAawwpGk/lstm_dashboard?orgId=1')


if __name__ == "__main__":
    app.debug = True
    app.run()
    # main()
